/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Filename : main.spec.js
 *  Description:
 *  ----------------------------------------------------------------
 */

var main = require('../../scripts/main');

require('angular-mocks');

var ngModule = window.module;
var ngInject = window.inject;

/*----------------------------------------------------------------*/
/* ${module.name} testing
/*----------------------------------------------------------------*/
describe('${module.name} suite test ', function() {
    var lpModuleProvider;
    /*----------------------------------------------------------------*/
    /* Mock modules/Providers
    /*----------------------------------------------------------------*/
    beforeEach(ngModule(main.name, function(_lpModuleProvider_) {
        lpModuleProvider = _lpModuleProvider_;
    }));

    /*----------------------------------------------------------------*/
    /* Main
    /*----------------------------------------------------------------*/
    describe('Main', function() {
        it('should be an object', function() {
            expect(main).toBeObject();
        });

        it('should contain a provider', ngInject(function() {
            expect(lpModuleProvider).toBeObject();
        }));

        it('should contain a provider service', ngInject(function(lpModule) {
            expect(lpModule).toBeObject();
        }));

        it('should contain a utils constant', ngInject(function(lpModuleUtils) {
            expect(lpModuleUtils).toBeDefined();
        }));
    });

});


