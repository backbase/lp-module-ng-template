define(function(require, exports, module) {
    'use strict';

    /**
     * Services
     * @param  {DI services list}
     * @return {object}
     * @ngInject
     */
    function Services(MyService) {

        // registered services
        var services = {
            'my-service': MyService
        };

        return {
            api: function(name) {
                if (typeof services[name] === 'undefined') {
                    throw new Error('Unknown service ' + name + ' !!!');
                }
                return services[name];
            }
        };
    }

    /**
     * lpModule provider
     * @return {object} angular provider
     * @ngInject
     */
    exports.lpModule = function() {
        this.$get = Services;
    };
});
