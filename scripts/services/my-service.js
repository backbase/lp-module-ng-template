define(function(require, exports, module) {

    'use strict';

    // base utilities
    var utils = require('base').utils;
    // exception error
    var ExceptionError;
    // default configuration
    var defaults = {
        url: ''
    };
    // exposed api

    var api = {};
    /*----------------------------------------------------------------*/
    /* Private Functions
    /*----------------------------------------------------------------*/
    function parse(res) {
        return res.data;
    }

    function normalize(data) {
        return data
    }
    /**
     * @ngInject
     * @constructor
     */
    var MyService = function MyService($http, lpCoreError) {
        ExceptionError = lpCoreError.createException('MyService');
        this.fetch = $http;
    };

    // Add to lpCoreApiService
    api.setConfig = function(options) {
        this.config = utils.chain(options)
            .mapValues(utils.resolvePortalPlaceholders)
            .defaults(defaults)
            .value();
        return this;
    };

    // Add to lpCoreApiService
    api.getConfig = function(prop, defVal) {
        if (prop && utils.isString(prop)) {
            return this.config[prop] || defVal;
        } else {
            return this.config;
        }
    };
    // fetch data
    api.getAll = function(params) {
        return this.fetch({
                url: this.config.url
            })
            .then(parse)
            .then(normalize);
    };

    /**
     * mixing public api methods
     */
    utils.assign(MyService.prototype, api);

    // export service
    exports.MyService = MyService;

});
